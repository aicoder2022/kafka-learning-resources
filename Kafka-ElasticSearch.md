# Kafka and ElasticSearch

- [Install ElasticSearch (native)](https://www.elastic.co/downloads/elasticsearch)
- [Cluster health](https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-health.html)
        Port: 9200 or 9300?
- [Quick Guide (native or docker)](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html)
- Elastic Search output after successful running of the instance:
 ```bash
——————————————————————————————————————————————————————————————

--------------------------------------------------
-> Elasticsearch security features have been automatically configured!
-> Authentication is enabled and cluster connections are encrypted.

->  Password for the elastic user (reset with `bin/elasticsearch-reset-password -u elastic`):
  <password>

->  HTTP CA certificate SHA-256 fingerprint:
  5c6e797f4df5b7fd84755e4770e697cfda2befb436a9e1cec952e9a3cf3b863b

->  Configure Kibana to use this cluster:
* Run Kibana and click the configuration link in the terminal when Kibana starts.
* Copy the following enrollment token and paste it into Kibana in your browser (valid for the next 30 minutes):
  
  <enrollment token>

-> Configure other nodes to join this cluster:
* Copy the following enrollment token and start new Elasticsearch nodes with `bin/elasticsearch --enrollment-token <token>` (valid for the next 30 minutes):

  <enrollment token>

  If you're running in Docker, copy the enrollment token and run:
  `docker run -e "ENROLLMENT_TOKEN=<token>" docker.elastic.co/elasticsearch/elasticsearch:8.2.0`
--------------------------------------------------

—————————————————————————————————————————————————————————————— 
```
**Note:** api to create a topic with type and id wasn't successful, this [feature has been removed from version 7.x](https://www.elastic.co/guide/en/elasticsearch/reference/current/removal-of-types.html).

- [Installing ES and Kibana via Docker compose](https://www.youtube.com/watch?v=BYcXvhJTDpg)

- ELK on Docker: [1](https://github.com/deviantony/docker-elk) | [2](https://github.com/obegendi/Devbox) | [3](https://github.com/seyeon-shijuan/monitoring-system)

- [Creating an ES Index via Kibana](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html)

- Kafka connectors
    - [Confluent connectors](https://www.confluent.io/product/confluent-connectors/)
    - [Connectors](https://www.confluent.io/product/connectors/)
    - [Confluent Hub](https://www.confluent.io/hub/)
        - [Twitter Source Connector](https://www.confluent.io/hub/jcustenborder/kafka-connect-twitter)
        - [Kafka Twitter Connector on github](https://github.com/jcustenborder/kafka-connect-twitter)

- Confluent CLI
	- https://docs.confluent.io/confluent-cli/current/overview.html