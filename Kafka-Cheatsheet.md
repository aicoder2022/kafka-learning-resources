# Kafka cheatsheet

- Start zookeeper server
```bash
zookeeper-server-start.sh $KAFKA_CONFIG_DIR/zookeeper.properties
```

- Start Kafka server
```bash
kafka-server-start.sh $KAFKA_DIR/config/server.properties
```

- Create topic 
```bash
kafka-topics.sh --bootstrap-server 127.0.0.1:9092 --topic first_topic --create --partitions 3 --replication-factor 1
```

- Delete topic
```bash
kafka-topics.sh --bootstrap-server 127.0.0.1:9092 --topic first_topic --delete
```

- List topics
```bash
kafka-topics.sh --bootstrap-server 127.0.0.1:9092 --list
```

- Get more information about the consumer group
```bash
kafka-consumer-groups.sh --bootstrap-server 127.0.0.1:9092 --describe --group my-first-application
```

- Reset offsets for topic in consumer group: `--to-earliest`
```bash
kafka-consumer-groups.sh --bootstrap-server 127.0.0.1:9092  --group my-second-application --reset-offsets --to-earliest --execute --topic first_topic
```

- Reset offsets for topic in consumer group: `--shift-by [number]`
```bash
### shifts forwards by 2 (or any other value)
kafka-consumer-groups.sh --bootstrap-server 127.0.0.1:9092  --group my-second-application --reset-offsets --shift-by 2 --execute --topic first_topic

or

### shifts backwards by 2 (or any other value)
kafka-consumer-groups.sh --bootstrap-server 127.0.0.1:9092  --group my-second-application --reset-offsets --shift-by -2 --execute --topic first_topic
```
```bash
kafka-consumer-groups.sh --bootstrap-server localhost:9092  --group kafka-demo-elasticsearch --reset-offsets --execute --to-earliest --topic twitter_tweets
```

- Produce data, write to a topic
```bash
kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic first_topic --producer-property acks=all
```

- Consume data, read from a topic (under a consumer group)
```bash
kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic first_topic --group my-first-application
```

## Twitter example

- Create topic (twitter_tweets)
```bash
kafka-topics.sh --bootstrap-server 127.0.0.1:2181 --create --topic twitter_tweets --partitions 6 --replication-factor 1
```

- Consume data, read from a topic (twitter_tweets)
```bash
kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic twitter_tweets
```

- Produce data, write to a topic (twitter_tweets)
```bash
kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic twitter_tweets --producer-property acks=all
```