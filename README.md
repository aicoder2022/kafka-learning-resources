# Kafka Learning Resources

Below are a number of resources categorised to help with learning Kafka. I gathered them during my learning process and sharing it now with the wider community.

The markdown editor [Zettlr](https://www.zettlr.com/) was used to write and organise the markdown files in this repository, it's a simple and useful Markdown editor (available across multiple platforms including [macOS](https://www.zettlr.com/download/macos)). [Obsidian](https://obsidian.md/) is also another good alternative.

_**If you find anything useful related to Kafka, please feel free to create a pull request (called merge request on Gitlab) to this repo and place it under an appropriate category (new or existing).**_

## Table of contents
- [Prerequisites](#prerequisites): things you must have installed for kafka and other related tools to run
- [Quickstart](#quickstart): get started with Kafka
- [Notes](#notes): my notes gathered after following online courses 
- [Q and A](#q-and-a): questions I had when I came across something new or was stuck with a problem to solve
- [Blogs](#blogs): resources I read to setup kafka or fix a bug/issues/problem with Kafka or relatesd tools
- [Courses](#courses): list of courses on Kafka, I found was recommended by others
- [Testing](#testing): testing kafka and other tools from within your app
- [Monitoring Kafka](#monitoring-kafka): a whole separate topic on how to monitor kafka and related tools
- [Capturing Kafka logs](#capturing-kafka-logs): specially for capturing Kafka logs
- [Cheatsheet](#cheatsheet): one place stop for quick commands or tips or tricks when using Kafka
- [Docker](#docker): resources on how to run Kafka setups via `docker` or `docker-compose`
- [ElasticSearch](#elasticsearch): Kafka and ElasticSearch setup and other related resources
- [CI/CD on Gitlab/GitHub](#cicd-on-gitlabgithub): installing/building and running Kafka and related tools on CI/CD for e.g. gitlab
- [Misc things](#misc-things)

## Prerequisites
- Docker (latest version) | [Install](https://docs.docker.com/get-docker/)
- Download and install java 8: [[1](https://stackoverflow.com/questions/24342886/how-to-install-java-8-on-mac )] | [[2](https://gist.github.com/wavezhang/ba8425f24a968ec9b2a8619d7c2d86a6)]
- [Download and install java 17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)
- [Switching to Java on the macOS](https://medium.com/@devkosal/switching-java-jdk-versions-on-macos-80bc868e686a)
- [Install maven (for Java)](https://maven.apache.org/download.cgi)
- Python and pip on the macOS: [[1](https://www.python.org/downloads/macos/)] | [[2](https://phoenixnap.com/kb/install-pip-mac)]

## Quickstart
- Install using Docker (via docker-compose)
    - [Quickstart](https://developer.confluent.io/quickstart/kafka-docker/)
    - [Hello Kafka World! The complete guide to Kafka with Docker and Python](https://medium.com/big-data-engineering/hello-kafka-world-the-complete-guide-to-kafka-with-docker-and-python-f788e2588cfc)

## Notes
 - See [Notes from the OReilly Kafka Beginners course](Notes-from-the-OReilly-Kafka-Beginners-course.md)

## Q and A
- See  [Q&A](Kafka-Q&A.md)

## Books
- [Working with Kafka for Local Development](https://www.oreilly.com/videos/working-with-kafka/10000MNHV2021132/) (28m)  
- [Kafka: The Definitive Guide, 2nd Edition](https://learning.oreilly.com/library/view/kafka-the-definitive/9781492043072/) (14h+)
- [Apache Kafka 1.0 Cookbook]( https://learning.oreilly.com/library/view/apache-kafka-1-0/9781787286849/) (9h 37m)
- [Understanding Streaming Data with Apache Kafka (playlist)](https://www.oreilly.com/playlists/236da00f-6848-4a26-90c3-6dade6b41075/)
- [Designing Event-driven systems](https://www.oreilly.com/library/view/designing-event-driven-systems/9781492038252/) (3h 58m)
- [Kafka in Action](https://www.oreilly.com/library/view/kafka-in-action/9781617295232/) (7h 48m)
- [Kafka Streams in Action](https://www.oreilly.com/library/view/kafka-streams-in/9781617294471/) (8h 23m)
- [Apache Kafka A-Z with Hands-On Learning](https://www.oreilly.com/videos/apache-kafka-a-z/9781801077569/) (9h 36m)

## Blogs 
- [When NOT to use Apache Kafka?](https://kai-waehner.medium.com/when-not-to-use-apache-kafka-a35345226a9f)
- [AWS Kinesis versus Kafka](https://www.softkraft.co/aws-kinesis-vs-kafka-comparison/)
- [Effective Strategies for Kafka Topic Partitioning ](https://newrelic.com/blog/best-practices/effective-strategies-kafka-topic-partitioning)

## Courses
   - Video: [Kafka for beginners](https://learning.oreilly.com/videos/apache-kafka-series/9781789342604/) by Stephane Maarek (7h 28m)
   - Video: [Apache Kafka Series - Kafka Streams for Data Processing](https://learning.oreilly.com/videos/apache-kafka-series/9781789343496/) by Stephane Maarek (4h 46m)
   - Video: [Apache Kafka real-time stream processing](https://www.oreilly.com/videos/apache-kafka/9781800209343/) (10h 52m)
   - Video: [Apache Kafka streams on Udemy](https://www.udemy.com/course/kafka-streams/)  by Stephane Maarek (5h+)
   - Video: [Apache Kafka Series - Confluent Schema Registry and REST Proxy](https://learning.oreilly.com/videos/apache-kafka-series/9781789344806/) by Stephane Maarek (4h 18m)

## Testing 
- Testcontainers available for these components
    - [kafka](https://www.testcontainers.org/modules/kafka/)
    - [elasticsearch](https://www.testcontainers.org/modules/elasticsearch)
    - for other products (called modules on [testcontainers](https://www.testcontainers.org/)), go to https://www.testcontainers.org/ and uncollapse _Modules_ to find other modules

**[Back to Table of Contents](#table-of-contents)**

## Monitoring Kafka
- See [Monitoring kafka and zookeeper](Monitoring-kafka-and-zookeeper.md)

## Capturing Kafka logs
- [Configure Docker Logging](https://docs.confluent.io/platform/current/installation/docker/operations/logging.html)

## Cheatsheet
- See [Cheatsheet](Kafka-Cheatsheet.md)

## Docker
- [docker cp commands](https://www.edureka.co/community/17112/how-do-i-copy-a-file-from-docker-container-to-host)
- Kafka stack: docker and docker-compose
    - [Running kafka locally with Docker (docker-compose config)](https://github.com/conduktor/kafka-stack-docker-compose) 
    - [Docker compose config for Kafka from George](https://github.com/GeorgeVince/example-etl-vis/blob/main/docker-compose.yml)
    - [docker-compose and secrets](https://docs.docker.com/compose/compose-file/#secrets)
    - [docker-compose and .env file](https://docs.docker.com/compose/compose-file/#env_file)
    - [Running kafka locally with Docker](https://lankydan.dev/running-kafka-locally-with-docker)
- [Deleting docker networks](https://yallalabs.com/devops/how-to-remove-all-unused-docker-networks/)
- SSH into the box using the `docker exec` command:

         docker exec -it [container name or id] /bin/bash

**[Back to Table of Contents](#table-of-contents)**

## ElasticSearch
- See [ElasticSearch](Kafka-ElasticSearch.md)

## CI/CD on Gitlab/GitHub
- [No resources atm]

## Misc things
-  [Kafka Documentation](https://kafka.apache.org/documentation)
    - [Producer config](https://kafka.apache.org/documentation/#producerconfigs)
    - [Consumer config](https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html)
-  Github repos
      - [Kafka: example Streaming / Viz ETL](https://github.com/GeorgeVince/example-etl-vis)
      - [Bootstrapping a Kafka/KSQL/Postgres pipeline](https://github.com/Squiggle/kafka-ksql-postgres) (2 years old)
- Fake twitter server/client
    - Directly connect with twitter: [1](http://mike.teczno.com/notes/streaming-data-from-twitter.html) | [2](https://www.sitepoint.com/twitter-json-example/)
    - [Twitter client (nodejs)](https://www.npmjs.com/package/twitter-server)
    - [Twitter client (Python)](https://github.com/bear/python-twitter)
    - Twitter server from From twitter on github: [1](https://github.com/twitter/twitter-server) | [2](https://twitter.github.io/twitter-server/) | [Twitter SDK and API docs](https://developer.twitter.com/en/docs/twitter-api/tools-and-libraries/v2)
- Other resources
     - [Kafka: The Definitive Guide v2 | Confluent](https://www.confluent.io/resources/kafka-the-definitive-guide-v2)
     - [Designing and testing a highly available Kafka cluster on Kubernetes](https://learnk8s.io/kafka-ha-kubernetes)
     - Kafka streaming in python using [faust-streaming](https://github.com/faust-streaming ), repo [faust](https://github.com/robinhood/faust) is not active anymore
     - **Open Data**: https://opendatahub.io/ (Open Data Hub is a blueprint for building an AI as a service platform on Red Hat's [Kubernetes](https://kubernetes.io/)-based [OpenShift® Container Platform](https://www.openshift.com/) and [Ceph Object Storage](https://www.redhat.com/en/technologies/storage/ceph). It inherits from upstream efforts such as [Kafka/Strimzi](https://strimzi.io/) and [Kubeflow](https://www.kubeflow.org/), and is the foundation for Red Hat's internal data science and AI platform.)

**[Back to Table of Contents](#table-of-contents)**